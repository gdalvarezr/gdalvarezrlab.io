<img src="https:/tyrlabx.com/assets/logos/logo_300x150.png" title="" alt="logo_500x300.png" width="203">

# Estimado cliente, a continuación los Términos y Condiciones de Uso

## (Licencia del software)

### Términos y Condiciones

Los términos expresados en este contrato constituyen las condiciones de uso y privacidad para todos los servicios prestados en la actualidad y los añadidos en el futuro, por TYR en su sitio web https://tyrlabx.com y su aplicación app.tyrlabx.com.

El usuario acepta, al momento de empezar a utilizar el servicio, respetar todas las condiciones impuestas por este contrato.

### Términos Usados

- Las definiciones a continuación tendrán el siguiente significado en este contrato:

- El término "contrato" o "acuerdo" hace referencia a este contrato y a sus Términos y Condiciones.

- El término "leyes aplicables" hace referencia a las leyes que se aplican en Colombia para este tipo de contratos.

- El término "sitio" ó "sitio web" hace referencia al sitio donde se prestan todos los servicios que TYR ofrece, sin tener en cuenta posibles terceros relacionados con TYR.

- El término "servicio" o “servicios” hace referencia a la aplicación que ofrece TYR en su sitio web y que opera desde el dominio app.tyrlabx.com. También hace referencia a el uso de la página web de TYR y su centro de contacto@tyrlabx.com

- El término "TYR", "Sistema TYR", "nosotros", "nuestro(a)" hace referencia a TYR Tecnología y resultados y todos sus asociados.

- El término "Sistema" hace referencia a la plataforma web usada por TYR para prestar su servicio, incluido el software que usa la plataforma y todos sus contenidos.

- El término "Aplicación" hace referencia al software que ofrece TYR como servicio a en su sitio app.tyrlabx.com

- El término "titular de la cuenta" hace referencia a la persona a nombre de quien está la cuenta registrada en TYR.

- El término "usuario" hace referencia a la persona que visita y usa el sitio web de TYR y su aplicación, sea o no "titular de cuenta"

- El término "Plan" hace referencia a las condiciones de uso que tiene un usuario según el pago que haya efectuado.

### Términos y condiciones

#### Aceptación

El usuario de este sitio, acepta por simple causal de uso del sistema TYR lo dispuesto en este contrato y en sus Términos y Condiciones, susceptible de cambio sin previo aviso por parte de TYR. Si el usuario representa una organización, está dando por entendido que la organización acepta ceñirse a este contrato y que tiene las facultades para actuar en nombre de aquella y por lo tanto obligarla frente a TYR y aceptar las obligaciones establecidas en el presente contrato. El usuario que no esté de acuerdo con esto, no podrá hacer uso de los servicios prestados por TYR.

Capacidad de Celebración de Contratos

Según la ley de Colombia, el usuario que acepte este acuerdo de términos y condiciones, debe ser legalmente apto para celebrar un contrato según lo permita su autonomía de la voluntad y las leyes que le sean aplicables. Refiérase entonces, el que quiera celebrar este contrato a la teoría general de celebración de contratos de la ley que aplica en su país.

TYR da por entendido que aquel usuario que acepte este acuerdo de términos y condiciones conoce previamente si está o no en capacidad de celebrar contratos a nombre de la persona que se determine como Usuario. Quienes sean considerados incapaces absolutos o relativos o parciales deberán tener autorización de sus representantes legales para celebrar este contrato, y serán estos últimos considerados responsables de cualquier conducta de sus apoderados.

## Registro de Cuentas y Usuarios

TYR presta un servicio de software que se accede a través de su sitio web https://tyrlabx.comLos usuarios que accedan a este servicio deberán registrar una cuenta y brindar la información solicitada en los formularios que se habilitan a la hora de registrar una cuenta. TYR da por entendido que cualquier información ingresada en estos formularios es hecha bajo juramento y por lo tanto exonera a TYR de poseer información falsa sobre cualquier usuario.

TYR permite el ingreso de una cantidad limitada de usuarios según el plan que cada titular o usuario de cuenta haya elegido al momento de registrar su cuenta. TYR no permite, bajo ninguna circunstancia, que estos usuarios sean distintos de aquellos que el titular de la cuenta haya decidido habilitar como tales para el uso de la plataforma y estos no podrán ser reemplazados por otras personas que conozcan las credenciales para ingresar a la plataforma. De suceder una situación como ésta, TYR no se hace responsable por el uso pernicioso de la información de la cuenta en contra del titular de la misma ni frente al usuario ni frente a terceros.

## Descripción del Servicio

TYR presta el servicio de su aplicación web en arrendamiento por propiedad intelectual. El usuario adquiere una licencia no-exclusiva, mundial y temporal e intransferible para usar el sistema TYR según las condiciones del plan que haya elegido al momento de pagar por el servicio, o si es el caso acepta las condiciones que tiene al ser un usuario gratis pero en ningún momento adquiere propiedad sobre la plataforma.

Los derechos de autor sobre las obras de software que componen la plataforma y los Servicios serán de titularidad de TYR y bajo ninguna interpretación de éstos términos de servicio se entenderán transferidos al usuario.

El servicio de TYR se inicia al momento de registrar una cuenta en el sitio web de TYR, tras haber aceptado las condiciones expresadas en este contrato. El servicio consiste en el uso del software TYR disponible en app.tyrlabx.com y todos los servicios disponibles en sitios web asociados.

El usuario se hace conocedor de los servicios por los que está pagando a la hora de usar alguno de los planes que ofrece TYR. TYR no se hará responsable en ningún caso por los errores cometidos por el usuario a la hora de elegir su plan, así como tampoco al momento de digitar o ingresar su información tanto personal como de la operación y marcha de su actividad empresarial ni tampoco de la clasificación que realice de la misma lo que afectará irremediablemente los resultados arrojados por el Software.

TYR da por entendido que el titular de la cuenta y los usuarios conocen cualquier cambio que se haga en la configuración de ésta, así como que realizará los cambios que considere pertinentes. TYR no se responsabilizará por la pérdida de información que ocurra por fuerza mayor, caso fortuito o hecho de un tercero tal y como se explica bajo el título “Operación del Sitio”.

TYR garantiza al Usuario el libre acceso al sitio web https://app.tyrlabx.com para ver la información allí disponible disponible, bajo condiciones normales, sin embargo el usuario acepta que existan circunstancias técnicas por las que ésta información puede llegar a estar inaccesible de manera temporal y por lo tanto exonera a TYR de cualquier tipo de responsabilidad por este hecho, bajo el entendido de que esto puede obedecer a limitaciones inherentes al estado de la tecnología en la actualidad.

## Alcance y nivel de servicio

En condiciones normales, la plataforma de TYR tiene un “down time” de aproximadamente 5%, de modo que la plataforma se encuentra disponible el 95%  del tiempo. TYR adelantará las gestiones que a su juicio estime conducentes, para que la aplicación esté disponible para el usuario, pero no garantiza lo anterior, por cuanto pueden existir eventos como daños en las comunicaciones, actos de terceros, mantenimiento o reestructuración de la aplicación, entre otros, que escapen al control y responsabilidad de TYR. No obstante lo anterior, TYR garantiza a los usuarios que la aplicación estará disponible en circunstancias normales, según lo anunciado, desde cualquier lugar del mundo que permita una conexión a Internet, siempre y cuando el usuario recuerde las credenciales de su cuenta y las condiciones de prestación del servicio de conexión a Internet por parte de cada proveedor, lo permitan.

De manera tal que si no se pudiera acceder por el tipo de conexión o por el deficiente servicio del mencionado proveedor, ello no implica en ningún momento incumplimiento de la prestación del servicio prestado por parte de TYR.

En cuanto al mantenimiento y reestructuración de la aplicación, TYR se reserva el derecho de llevar a cabo las gestiones necesarias, sin previo aviso a los usuarios, pero procurará que se les brinde un aviso con la antelación que a su juicio considere conveniente o prudente para evitar incomodidades o eventuales perjuicios en el procesamiento de datos o de información.

## Medios de pago

El usuario podrá elegir el plan y periodo de pago al momento de realizar el primer pago. Según el periodo elegido: mensual o anual, el próximo cobro se realizará el primer día del mes en que se inscribió.

El pago de TYR es anticipado y el usuario acepta que se realicen cobros recurrentes según los periodos elegidos. Además se compromete a notificar a TYR en caso tal de que no quiera que se genere el siguiente cobro, mínimo 2 días hábiles antes de sur próxima fecha de cobro.

En caso de que el usuario no notifique a TYR, con anticipación sobre su deseo de no generar el cobro recurrente, este tendrá 5 días hábiles para pedir la devolución del dinero conforme el derecho de retracto previsto en el Estatuto del Consumidor.

TYR ofrece los medios de pago que se reseñan en su página web. Sin embargo, para acceder a TYR a través de algunos de estos sistemas de pago, puede ser necesario registrar por parte del titular de la cuenta alguna información, como por ejemplo un número de cuenta o  tarjeta de crédito expedida por parte de una entidad financiera o que se encuentre vigente, para poder realizar el cobro por los servicios prestados en nuestro software online disponible en app.tyrlabx.com o en la . Si existen retenciones de cualquier tipo estas tendrán que ser asumidas por el usuario.

No obstante esto, el usuario autoriza a TYR a llevar a cabo cualquier verificación o consulta de los datos suyos en cualquier base de datos, centrales de información y riesgo y cualquier otra fuente de información que le brinde historia sobre la forma en la que atiende sus compromisos financieros y crediticios, tanto en el ámbito nacional como en el internacional.

El usuario afirma bajo la gravedad de juramento, que no está incluido en la Lista Clinton y que sus ingresos no tienen relación con ningún tipo de actos de aquellos que la legislación colombiana e internacional determinan para el lavado de activos, narcotráfico, terrorismo ni otro delito y que son adquiridos de manera lícita.

## Información de la cuenta

TYR es un aplicativo Web, el cual proporciona un sitio donde se alojan datos que el usuario **administra bajo su cuenta** y riesgo. TYR vela por mantener la información de los usuarios, segura y toma las precauciones a su juicio necesarias para ello, más no se responsabiliza por actos mal intencionados de terceros y las consecuencias de ello frente al usuario o frente a terceros.

TYR pone en conocimiento de sus usuarios, que la información que suministre se albergará en servidores de terceros, específicamente Linode aka AKAMAI que cumplen con los más altos estándares de seguridad e idoneidad.

TYR tampoco se hace responsable por el tipo de información ingresada por cada usuario en su cuenta ni por los resultados inadecuados si la misma se ingresó de manera inadecuada de acuerdo con los parámetros contables y fiscales aplicables a la materia en cada caso. Se da a entender que el usuario, al usar los servicios de TYR, hará un uso sano y legal de todas las herramientas que se ponen a su disposición y esto exonera a TYR de cualquier uso indebido de su información por parte de cualquier usuario, entendiéndose por ello el usar información para evadir obligaciones tributarias, entre otras.

TYR no estará obligada a velar por la legalidad del contenido e información que los usuarios alberguen en su cuenta a través de los Servicios prestados, sin embargo podrá tomar los correctivos en contra de información ilegal, cuando lo considere pertinente.

El usuario se obliga a la hora de usar cualquiera de los servicios de TYR a (i) no causar daño físico, moral o mental a otros usuarios del servicio (ii) no utilizar el servicio con fines maliciosos o de mala voluntad, ni para beneficiarse en perjuicio de terceros y mucho menos del Estado entendiendo por éste, aquel que de acuerdo con la normatividad que le sea aplicable, sea quien deba recibir dinero por concepto de impuestos bajo cualquier denominación por el resultado de la operación mercantil del usuario. (iii) no usar el servicio con fines criminales o ilegales ni para sacar provecho o beneficio ilícito para sí o para terceros, bien sea remunerado o no, (iv) no publicar información que vulnere derechos de terceros, tales como derechos de propiedad intelectual, secretos industriales o cualquier otro que sea de propiedad de terceros y respecto de los cuales no se encuentre autorizado (v) publicar información  información sensible que ya no es vigente o que pueda inducir a error a terceros o al Estado

El usuario de TYR manifiesta ser dueño de la información que ingresa en el sistema y bajo ninguna circunstancia esta información pasará a ser propiedad de TYR Tecnología y resultados ., y de manera inversa, esta última sociedad es la única dueña de la plataforma sobre la cual el usuario ingresa la información , sin que respecto de ésta se considera surtida transferencia alguna en razón de éste contrato. Si el usuario da por terminado el contrato tendrá la información a su alcance, pero en ningún momento TYR se obliga a entregar la misma en formato alguno ni a llevar a cabo ningún tipo de proceso de migración, ni mucho menos a efectuar un desarrollo para que la información pueda ser analizada, ingresada o digitalizada en cualquier otro software, pues sólo se ingresan datos, para su consulta y procesamiento en aras de su funcionamiento y uso.

Adicional a lo anterior, TYR **no se responsabiliza por el mal diligenciamiento de un formulario o por la información que erróneamente suministre el USUARIO al momento de realizar un trámite o de ingresar la información al software.**

## Política de privacidad

### Modo de uso de las cookies

TYR, Mediante la instalación de cookies e identificadores propios y de terceros y a través de herramientas para el análisis de uso de cuenta, podrá realizar actividades de seguimiento a los usuarios que utilizan su aplicación. Con estas herramientas, TYR podrá recopilar información de cada usuario para el análisis de estadísticas e implementación de estrategias de comunicación y publicidad. Estas herramientas podrán realizar seguimiento de la configuración del usuario y hacen que su experiencia en la aplicación sea más práctica, reconociendo y recordando sus preferencias y ajustes.

En esta medida, TYR podrá recopilar datos de uso, como la duración de uso, o datos demográficos como el origen, el sexo y la edad. TYR usa esta información para fines analíticos. El usuario de TYR podrá deshabilitar estas herramientas.

Las actividades de seguimiento adelantadas por TYR también podrán realizarse dentro de la aplicación, para análisis interno, ejemplo, pero no exclusivamente para determinar tendencias y participación en la aplicación, tiempo de uso, preferencial, entre otros. Estas herramientas podrán ser desactivadas por el usuario para conservar su privacidad.

### Análisis de datos.

El usuario al crear su cuenta y hacer uso de la aplicación TYR, acepta que LA EMPRESA puede suministrar, transferir, compartir y analizar los datos del giro ordinario de su negocio, con empresas afiliadas, filiales, vinculadas o subordinadas de LA EMPRESA en Colombia o cualquier otro país, esta información podrá ser usada para fines comerciales por parte de los terceros.

El usuario en cualquier momento puede solicitar a LA EMPRESA que detenga de forma indefinida el proceso de compartir sus datos con terceros, para esto debe realizar su solicitud al correo contacto@tyrlabx.com o comunicarse a través del chat de atención al cliente.

## Política Comunicacional

Al registrar una cuenta con TYR el titular de la cuenta debe introducir un email y un teléfono de contacto, así como registrar todos aquellos datos que el software le solicite a fin de determinar no solo su identificación, sino aquello que le permita al software procesar la información que el mismo usuario diligencie o ingrese. Al hacer esto, acepta recibir todos los correos y llamadas con información sobre el uso de TYR, información promocional u otro tipo de información procesada o enviada por TYR.  No obstante lo anterior, TYR no podrá utilizar en beneficio suyo o de terceros, la información que haya ingresado el usuario, sin embargo, podrá utilizar la base de datos de las empresas o usuarias con fines publicitarios o comerciales para sí o para terceros, sin que por ello deba participar de ingreso alguno a los usuarios. El usuario podrá solicitar que sus datos dejen de ser compartidos en cualquier momento y por medio de los canales establecidos y/o a la siguiente dirección de correo electrónico: contacto@tyrlabx.com

Esta cláusula se interpretará sin detrimento de la autorización de uso de información a que se refiere el título “Política de Privacidad”. La autorización a que se refiere ésta cláusula podrá ser revocada por el usuario en cualquier tiempo escribiendo a la siguiente dirección de correo electrónico: contacto@tyrlabx.com

Adicionalmente los usuarios tendrán habilitada la posibilidad de comunicarse con el equipo de TYR en caso de que tengan preguntas o dudas por resolver, preguntas generales sobre su cuenta o sobre el software como tal. Para los casos anteriores, deberán escribir a contacto@tyrlabx.com. o utilizar la pestaña de soporte disponible en la aplicación.

## Seguridad de la Cuenta

TYR vela por la seguridad de la contraseña que provea el titular de cuenta y los usuarios a la hora de registrar una cuenta con TYR y garantiza que tomará las medidas que se encuentren a su alcance para que esta contraseña no sea vista por terceros, mas no puede arrogarse la responsabilidad de garantizar su confidencialidad. Por otro lado TYR no se responsabiliza por el mal uso de la contraseña por parte del usuario ni por el uso de contraseñas que sean fáciles de descifrar, asumiendo que siempre que se acceda al sistema, lo hace el usuario directamente.

## Operación del Sitio

TYR llevará a cabo las gestiones que a su juicio estime conducentes, tendientes a que los sitios webs asociados siempre estén disponibles para el usuario, pero no garantiza lo anterior, tanto por los daños en las comunicaciones, por actos de terceros, mantenimiento o reestructuración de los sitios u otro tipo de actos que se escapen del alcance y responsabilidad directa de TYR. No obstante lo anterior, TYR garantiza a los usuarios que pagan por el servicio, que el software estará disponible en app.tyrlabx.com desde cualquier lugar del mundo que permita una conexión a Internet, siempre y cuando el usuario recuerde las credenciales de su cuenta y las condiciones de prestación del servicio de conexión a Internet por parte de cada proveedor, lo permitan, lo cual escapa de las obligaciones de TYR, de manera tal que si no se pudiera acceder por el tipo de conexión y las características que le sean propias, por fallas en la comunicación o por el deficiente servicio del mencionado proveedor, ello no implica en ningún momento incumplimiento de la prestación del servicio contratado por parte de TYR. En cuanto al mantenimiento y reestructuración del sitio, TYR se reserva el derecho de hacerlo sin previo aviso a los usuarios, pero procurará que se les brinde un aviso con la antelación que a su juicio considere conveniente o prudente para evitar incomodidades o eventuales perjuicios en el procesamiento de datos o de información.

### Limitaciones

EL CLIENTE no podrá aplicar técnicas de ingeniería inversa, descompilar o desensamblar el software, ni realizar cualquier otra operación que tienda a descubrir el código fuente. Además queda prohibida la separación de los componentes. TYR autoriza el uso del software como un producto único. Las partes que lo componen no se podrán separar para utilizarlas en más de aquellas unidades o estaciones de trabajo en las que lo instale LA EMPRESA, ni hacer uso de ellas por separado. El usuario comprende que faltar a lo dispuesto en ésta cláusula constituye un delito de acuerdo al artículo 272 del Código Penal Colombiano.

## Interrupción y Terminación del Servicio

TYR se reserva el derecho de terminar el servicio en cualquier momento, tanto de manera permanente como temporal..

TYR podrá terminar de manera unilateral la cuenta de un usuario en los siguientes escenarios: (i) En caso de que el USUARIO utilice los servicios prestados por TYR para fines contrarios a la ley, especialmente aquellos que contraríen derechos de propiedad intelectual de terceros y sobre de todos de TYR y de otros usuarios; (ii) En caso de que TYR encuentre que el USUARIO está haciendo uso de su cuenta para la transmisión de programas malignos como virus, malwares, spywares, troyanos o similares, que puedan comprometer el debido funcionamiento de la plataforma de TYR o que perjudiquen a terceros; (iii) Cuando existan elementos que permitan inferir a TYR que el USUARIO no cuenta con la edad mínima para contratar los Servicios, en los términos del artículo segundo de estas. (iv) Cuando el usuario no haya seguido los pasos indicados de forma correcta o use una comunicación inadecuada con TYR.

## Condiciones.

TYR se reserva el derecho a decidir si el contenido publicado por los usuarios, al igual que el material de texto o fotográfico que sea cargado a la página web de TYR resulta apropiado y se ajusta a las Condiciones. En éste sentido, TYR podrá impedir la publicación y comercialización de contenido que infrinja derechos de imagen, de habeas data y de privacidad de terceros, así como aquellos que resulten ofensivos, difamatorios o que constituyan infracciones a la ley.

## Parágrafo: Suspensión Del Servicio.

LA EMPRESA se reserva el derecho de suspender la prestación de los servicios al USUARIO y de inhabilitar su acceso al Software, así como a cualquiera de los módulos creados para EL USUARIO si luego de dos intentos de cobro del servicio el recaudo resulta fallido, o en caso de no recibir el pago del servicio en la forma acordada.

Se dará la suspensión del servicio al USUARIO sin aviso anticipado.

En caso de suspensión, la cuenta del USUARIO no será reactivada hasta que el pago de todas aquellas deudas que en ese momento se encuentren a cargo del usuario se hayan realizado por completo. Si luego de tres intentos de cobro del servicio el recaudo resulta fallido, el USUARIO debe realizar el pago del dinero adeudado mediante un enlace de pagos. Para obtener el enlace donde se debe realizar el pago, el USUARIO debe comunicarse con el equipo de soporte de TYR escribiendo a 

contacto@tyrlabx.com

La suspensión de la cuenta del USUARIO no elimina su obligación de pagar las deudas pendientes, así como tampoco el porcentaje cobrado impide la generación de los gastos de cobranza e indemnización de perjuicios que se pudiesen generar a favor de TYR por tal incumplimiento.

En caso de falta de pago, o uso indebido de la cuenta, el usuario recibirá a su correo una notificación a partir de la cual tendrá un periodo de gracia de 15 días durante el cual podrá consultar su cuenta y la información que hasta el momento se encuentre almacenada en ella, pero sin la posibilidad de agregar nueva información. El usuario será responsable de copiar o sustraer dicha información, por cuanto una vez venza el periodo de 15 días, TYR dejará de ser responsable de la información albergada en la cuenta y esta podrá ser eliminada libremente.

## Legislación Aplicable y Jurisdicción

Este contrato se rige por las leyes de la República de Colombia. Si cualquier parte de este contrato se declara nula o contraria a la ley, entonces la provisión inválida o no exigible se considerará sustituida por una disposición válida y aplicable que más se acerque a la intención del contrato original y el resto del acuerdo entre TYR y el usuario continuará en efecto. A menos que se especifique lo contrario en este documento, estas Condiciones constituyen el acuerdo completo entre usted y TYR con respecto a los Servicios de TYR y reemplaza a todas las comunicaciones previas y propuestas, tanto de manera electrónica, oral o escrita, entre el usuario y TYR con respecto a los Servicios de TYR.

## Derecho de retracto.

El USUARIO podrá ejercer su derecho al retracto en los términos del artículo 47 de la Ley 1480 de 2011, es decir, podrá solicitar que se reverse la transacción perdiendo el dominio sobre su cuenta y recibiendo la devolución de lo pagado. Para efectos de poder ejercer el derecho de retracto será necesario que el USUARIO lo ejerza dentro de la oportunidad legal, es decir durante los cinco (5) días posteriores a la celebración del contrato.

## Interrupción del servicio

En caso de interrupción del pago por un período de cinco (5) meses, el sistema será automáticamente cancelado y la información del usuario será eliminada. Para reactivar el servicio, el usuario deberá pagar una tarifa de reconexión equivalente a un (1) mes del plan contratado. Si el usuario no realiza el pago dentro del plazo de un (1) mes posterior a la suspensión del servicio, la información será eliminada de forma permanente y no podrá ser recuperada.

## Comentarios y Preguntas

En caso de existir consultas, quejas o reclamos sobre la información contenida en el presente documento de Términos y Condiciones, por favor escribir al  correo contacto@tyrlabx.com, utilizando como asunto: Comentario Términos y Condiciones.

## Política de Tratamiento de Datos Personales TYR Tecnología y resultados .

En cumplimiento a lo dispuesto en la Ley 1581 de 2012.  el Decreto 1377 de 2013 y el Decreto 886 de 2014, TYR Tecnología y resultados ., ha implementado la presente POLÍTICA DE TRATAMIENTO DE DATOS PERSONALES, con el fin de impartir un debido tratamiento a la información proporcionada dentro de cualquier registro de datos personales, realizado directamente ,  a través de la página https://tyrlabx.com o el aplicativo móvil denominado TYR. Política que contempla el manejo de solicitudes de peticiones, consultas y reclamos relacionados con el tratamiento de este tipo de información.

La presente Política de Tratamiento de Datos Personales está dirigida a todas las personas naturales  que tengan o hayan tenido alguna relación con TYR Tecnología y resultados ., y que sus datos personales se encuentren en sus bases de datos.

I. Identificación

Software https://app.tyrlabx.com
Correo electrónico: contacto@tyrlabx.com
Página Web: www.tyrlabx.com

II. Marco Legal

La presente política de Tratamiento de datos personales es elaborada de conformidad con lo dispuesto en la Constitución Política, la Ley 1581 de 2012, el Decreto 1377 de 2013, el Decreto 1074 de 2015 y demás disposiciones complementarias que las modifiquen, adicionen o complementen las cuales serán aplicada por TYR Tecnología y resultados . respecto de la recolección, almacenamiento, uso, circulación, supresión y de todas aquellas actividades que constituyan tratamiento de datos personales.

Los siguientes son los  principios rectores del Tratamiento de sus Datos Personales y LA EMPRESA como entidad respetuosa de la ley acatará: Principio de Legalidad,  Principio de Finalidad, Principio de Libertad, Principio de Veracidad o Calidad, Principio de Transparencia,  Principio de Acceso y Circulación Restringida, Principio de Temporalidad del dato,  Principio de Seguridad y  Principio de Confidencialidad.

III. Definiciones

Para efectos de la interpretación y aplicación  de la presente política solicitamos tener en cuenta las siguientes definiciones

IV. Finalidad

A través del presente documento, se establece el Manual Interno de Políticas y de Procedimientos para el Tratamiento de Datos Personales de TYR Tecnología y resultados ., el cual tendrá como alcance  la aplicación de todas las bases de datos  donde se almacene información personal y sea objeto de tratamiento por parte de la compañía, en adelante TYR o LA EMPRESA.

TYR entiende por protección de datos todas aquellas medidas tomadas a nivel  fisico, tecnico y juridico para garantizar que la información de los Titulares - personas naturales - (clientes, empleados, ex empleados, proveedores, acreedores, etc.) , esté segura de cualquier ataque o intento de acceder a ella por parte de personas no autorizadas así como que su uso y conservación sea adecuado.

TYR podrá hacer uso de los datos personales para:

a) Ejecutar la relación contractual existente con sus clientes, proveedores  trabajadores y aspirantes, incluido el pago de obligaciones contractuales

b) Proveer los servicios y/o los productos requeridos por sus usuarios

c) Informar sobre nuevos productos o servicios y/o sobre cambios en los mismos

d) Evaluar la calidad del servicio

e) Realizar estudios internos sobre hábitos de consumo

f) Enviar al correo físico, electrónico, celular o dispositivo móvil, vía mensajes de texto (SMS y/o MMS) o a través de cualquier otro medio análogo y/o digital de comunicación creado o por crearse, información comercial, publicitaria o promocional sobre los productos y/o servicios, eventos y/o promociones de tipo comercial o no de estas, con el fin de impulsar, invitar, dirigir, ejecutar, informar y de manera general, llevar a cabo campañas, promociones o concursos de carácter comercial o publicitario, adelantados por TYR y/o por terceras personas

g) Desarrollar el proceso de selección, evaluación, y vinculación laboral

h) Soportar procesos de auditoría interna o externa

i) Registrar la información de empleados y/o pensionados (activos e inactivos) en las bases de datos de TYR:   Los indicados en la autorización otorgada por el titular del dato o descritos en el aviso de privacidad respectivo, según sea el caso

j) Suministrar, compartir, enviar o entregar sus datos personales a empresas filiales, vinculadas, o subordinadas de TYR ubicadas en Colombia o cualquier otro país en el evento que dichas compañías requieran la información para los fines aquí indicados.

V. Base de Datos

A. Las bases de datos en las que la empresa TYR Tecnología y resultados . actua como responsable y encargada del tratamiento:

En el tratamiento de los datos contenidos en las siguientes bases de datos TYR actúa tanto en calidad de Responsable, toda vez que son quienes recaudan la información y toman las decisiones sobre el tratamiento de los datos, como en calidad de Encargada, en la medida que es quien realiza el Tratamiento de datos.

A continuación se mencionan las 4 bases de datos sobre las cuales TYR Tecnología y resultados . realiza el tratamiento de información personal:

Base de datos de clientes:

Descripción: Esta base de datos corresponde a la información de los clientes de LA EMPRESA, es decir corresponde a aquellas personas naturales que acceden a los diferentes servicios de TYR.

Contenido: Esta base de datos contiene la siguiente información personal: nombre, número de identificación, ciudad y país de residencia, dirección de correspondencia, correo electronico, telefono, actividad economica.

Forma en que se recopilan los datos: La información se obtiene de forma directa mediante inscripción de formulario a través de la página web o la aplicación móvil y finalmente se configura la cuenta, en todos los casos queda consignado en servidores especializados de Linode que cuenta con toda la infraestructura para garantizar la seguridad de la información. En la recolección de la información el cliente expresa la autorización de recolección de sus datos personales  por parte de TYR de acuerdo a los fines establecidos en la presente política publicada en la página web https://tyrlabx.comy en la respectiva aplicación.

Finalidad: La información contenida en esta base de datos tiene por  finalidad permitir a TYR cumplir con su objeto de prestación de servicios contables desde una plataforma en la nube. De manera que esta base de datos tiene las siguientes finalidades: Para contacto con el cliente en: Soporte técnico y servicio al cliente, para facturación: Soporte de facturación, gestión de cobros cobro y recaudo y para mercadeo y publicidad: Envío de boletines informativos, promociones, encuestas e información sobre nuevas funcionalidades

Tratamiento: La información personal contenida en esta base de datos es objeto de recolección, almacenamiento, uso, y de circulación.

Circulación interna: La información ingresada al sistema para el uso de los servicios prestados por TYR, pasa al área de soporte y servicio al cliente en caso de requerir.

Circulación externa:  La información contenida en esta base de datos  se guarda en un servidor de Linode y solo pueden acceder a ella ciertas personas de TYR autorizadas como el área de ingeniería y administrativa. De igual forma al Panel sólo puede acceder cada usuario, ya que es personal y cuenta con una contraseña y un token de seguridad para ello.  Para temas de facturación y cartera se cuenta con un proveedor de contabilidad, para el recaudo se utilizan plataformas de pago o se hace en entidades bancarias. Actualmente la autorización para el tratamiento de los datos personales, se obtiene a través de la página web app.tyrlabx.com, al igual que a través de la aplicación móvil desde el momento del registro, estos datos se amplían a la información financiera al momento de la suscripción y respectivo pago.

Vigencia: La información personal contenida en esta base de datos es custodiada mientras dura la relación comercial con el cliente más un periodo adicional de 10 años. Si en un caso individual, existen indicios de una necesidad de protección o de interés histórico de estos datos, se prolongará el plazo de almacenamiento hasta que se haya aclarado la necesidad legal de protección.

2. Bases de datos de empleados, ex empleados y aspirantes:

Descripción: Esta base de datos contiene la información que se recolecta sobre los aspirantes a algún cargo vacante dentro de LA EMPRESA a través de las hojas de vida, los empleados de la misma vinculados a través de contratos de trabajo y los estudiantes de práctica y las personas que dejaron de ser empleados de TYR. Esta base de datos es manejada por el área Administrativa y Gerencial de TYR.

Contenido: La información personal especificada en esta Base de Datos contiene los siguientes datos personales: Nombre, número de identificación, copia del documento de identificación, lugar de expedición, fecha y lugar de nacimiento, estado civil, ciudad de domicilio, dirección de residencia, teléfono, mail, salario, exámenes médicos de ingreso, tipo sanguíneo, fondo de pensiones, EPS, fondo de cesantías, datos familiares incluidos niños, niñas y adolescentes en primer grado de consanguinidad, registros fotográficos, ocupación, cargo, información académica, experiencia laboral, referencias laborales y personales, número de cuenta bancaria, entidad bancaria, fecha de retiro, datos de contacto en caso de emergencia, nombre y parentesco. La empresa recopila ciertos datos sensibles: Datos de menores, tipo sanguíneo y registros fotográficos.

Forma en que se recopilan los datos: El área Administrativa recoge la información por medio de las hojas de vida suministradas por el aspirante, en entrevistas de ingreso en presencia del aspirante y en los contratos laborales suscritos entre el empleado y LA EMPRESA.

Finalidad: Los datos anteriormente mencionados únicamente se recopilan con el fin de cumplir con las obligaciones derivadas del contrato laboral, dentro de los que están, la atención de solicitudes, la generación de certificados y constancias, la afiliación a las entidades del Sistema de Seguridad Social, Salud Ocupacional, la realización de actividades de Bienestar Laboral, el levantamiento de registros contables, los reportes a autoridades de control y vigilancia, contacto con el empleado, causar impuestos, comunicación en caso de ausencia, entre otros fines administrativos  y de contacto. Los datos sensibles son utilizados para la información relacionada con la salud del trabajador, se utiliza en casos de emergencia, o para el reporte ante las entidades del Sistema de Seguridad Social.

Tratamiento: El tratamiento que recibe esta base de datos por parte de LA EMPRESA en su condición de responsable y encargada incluye la recolección, el almacenamiento, el uso y la circulación. La circulación puede ser interna o externa y se efectúa de la siguiente forma:

Circulación Interna: Para pago de nómina y otros servicios, para  levantamiento de registros contables y para seguimiento a empleados, en cuanto a su labor y temas de salud ocupacional.

Circulación Externa: Según requerimiento de las siguientes entidades: Para la UGPP por Gestión Pensional y Parafiscales, para la DIAN, para el SENA y para el ICBF. Los datos también son utilizados para enviar reportes a las entidades administrativas que lo soliciten, con base en las normas del Sistema de Seguridad Social y complementarias. La información contenida en esta base de datos se guarda en archivos drive y en correo electrónico, y para acceder a ellos se requieren contraseñas las cuales solo son de conocimiento de las respectivas áreas que tienen acceso a la información, de igual forma se establecen políticas de confidencialidad.

Vigencia: Esta Base de Datos estará vigente mientras exista la relación laboral entre el empleado y LA EMPRESA, por 90 años más contados a partir de la fecha de liquidación del contrato. Así mismo LA EMPRESA conserva la información en un archivo histórico de las personas que han estado vinculadas a misma, así como de los aspirantes a los cargos que han estado vacantes.

3. Base de Datos de Proveedores

Descripción: Esta base de datos corresponde a la información personal  que se recolecta sobre las personas naturales proveedores que prestan servicios y venden productos a LA EMPRESA.

Contenido: Esta base de datos contiene la siguiente información: Nombre, número de identificación, dirección, teléfono, correo, numero de cuenta bancaria, titular de cuenta, rut y en caso de tenerlo sitio web.

Forma en que se recopilan los datos: La información se obtiene a través de la documentación suministrada por el proveedor con el fin de realizar el pago posterior a la facturación. Es importante resaltar que los proveedores han autorizado la recopilación de sus datos mediante diligenciamiento y aceptación de formulario.

Finalidad: La información contenida en esta base de datos tiene por finalidad crear al proveedor en  archivo Drive de TYR, cumplir con las obligaciones derivadas de la relación comercial, realizar pagos, realizar transferencias bancarias asociadas a los productos vendidos o servicios prestados, enviar reportes a DIAN, y a la Secretaría de Hacienda municipal y en general entidades tributarias, de conformidad con lo dispuesto en la normatividad colombiana y contactarlos para contratar de nuevo.

Tratamiento: La información personal contenida en esta base de datos es objeto de recolección, de almacenamiento, de uso y de circulación. La información contenida en esta base de datos se guarda en un archivo Drive de la compañía, por parte del área Administrativa. La autorización consta en un formulario electrónico, el cual es diligenciado y aceptado por los proveedores. Sobre las medidas de seguridad, al archivo Drive solo se puede acceder por personas autorizadas.

Vigencia: Esta base de datos se encuentra vigente mientras exista la relación comercial con el proveedor más un periodo de 10 años.

4. Base de datos Acrecer

Descripción: Contiene la información relacionada con las personas naturales que han decidido ser referidores de nuestra aplicación y por lo cual reciben un beneficio económico.

Contenido: La presente base de datos contiene la siguiente información: Nombre del referidor, correo electrónico del referidor, correo del referido, nombre del referido, teléfono, Rut y referencia bancaria.

Forma en que se recopilan los datos: El referido inscribe directamente sus datos en la plataforma de TYR e ingresa su referenciador con un código, por lo anterior en esta base de datos se encuentra la información de referidor y referido.

Finalidad: Cumplir con las obligaciones derivadas de la relación comercial, realizar pagos, realizar transferencias bancarias asociadas a los productos vendidos o servicios prestados, enviar reportes a DIAN, y a la Secretaría de Hacienda municipal y en general entidades tributarias, de conformidad con lo dispuesto en la normatividad colombiana y contactarlos para contratar de nuevo.

Tratamiento: La información personal contenida en esta base de datos es objeto de recolección, de almacenamiento, de uso y de circulación. La información contenida en esta base de datos se guarda en un archivo Drive de la compañía, por parte del área administrativa. La autorización consta en un formulario electrónico, el cual es diligenciado y aceptado por los proveedores. Sobre las medidas de seguridad, al archivo Drive solo se puede acceder por personas autorizadas.

Vigencia: Esta base de datos se encuentra vigente mientras exista la relación comercial con el proveedor más un periodo de 2 años.

B. Bases de datos en las que se recolectan Datos Especiales

Bases de Datos Sensibles

Para los efectos del manejo de los Datos Sensibles, LA EMPRESA  ha obtenido la correspondiente autorización de los Titulares cuya información reposa en sus Bases de Datos y obtendrá la autorización de manera previa, siempre que se trate de nuevos datos.

Para el tratamiento de los datos sensibles LA EMPRESA ha cumplido con las siguientes obligaciones:

Informó al Titular que por tratarse de Datos Sensibles, no estaba obligado a autorizar su tratamiento.

Informó al Titular cuáles de los datos son Sensibles y la finalidad del Tratamiento.

LA EMPRESA no condiciona ninguna actividad a que el Titular suministre Datos Sensibles.

2. Bases de datos Personal

Esta base de datos se describe en el literal A. del presente manual.

C. Registro de las Bases de Datos

De conformidad con la normatividad colombiana, las bases de datos anteriormente mencionadas serán inscritas en el Registro Nacional de Bases de Datos.

VI. Autorización del Titular para el Tratamiento de Datos Personales

LA EMPRESA por medio de la “Autorización para el Tratamiento de Datos Personales”   ha adoptado procedimientos para solicitarle al usuario, a más tardar en el momento de la recolección de sus datos personales, su autorización para el Tratamiento de los mismos e informarle las finalidades específicas del Tratamiento para las cuales se obtiene su consentimiento.

Al crear su cuenta, el usuario autoriza a LA EMPRESA a recolectar, suministrar, transferir, transmitir, analizar, verificar y usar sus datos personales y/o empresariales, datos financieros, contables, y en general aquellos datos relacionados con el giro ordinario de su negocio. Asimismo, autoriza a compartirlos siempre que sea necesario para procesos de pagos, estrategias comerciales u otros temas afines al negocio que busquen el beneficio del usuario, con proveedores, empresas filiales, vinculadas, subordinadas o aliadas de LA EMPRESA ubicadas en Colombia o en terceros países con el fin de que éstas puedan almacenarlos, analizarlos, usarlos, consultarlos y verificarlos ante operadores de información y riesgo, y contactar al usuario para enviarle información, hacerle ofertas comerciales de productos y servicios, o para el mantenimiento de los mismos.

Como Titular de información, el usuario acepta y entiende el derecho que le asiste a conocer, actualizar y rectificar sus datos personales, solicitar prueba de la autorización otorgada para su tratamiento, ser informado sobre el uso que se ha dado a los mismos, presentar quejas ante la SIC, revocar la autorización y/o solicitar la supresión de sus datos en los casos en que sea procedente y acceder en forma gratuita a los mismos.

La autorización otorgada por el Titular a LA EMPRESA, cumple con los requisitos exigidos en la legislación vigente aplicable, cuando ésta se manifieste: • Por escrito • De forma oral • Mediante conductas inequívocas del Titular que permitan concluir de forma razonable que éste otorgó a LA EMPRESA la autorización respectiva. En ningún caso su silencio será asimilado por LA EMPRESA como una conducta inequívoca.

LA EMPRESA ha establecido el correo electrónico soporte@TYR.com y el chat de atención al cliente, como los canales para que el Titular de los datos, pueda en todo momento solicitar como Responsable o Encargado del Tratamiento, la supresión de sus datos personales y/o revocar la autorización que nos ha otorgado para el Tratamiento de los mismos.

VII. Reglamento General de Protección de Datos RGPD/GDPR en inglés (General Data Protection Regulation)

TYR recopila y almacena toda la información de los usuarios. La cual se aloja en servidores especializados de Linode, únicamente se puede acceder a los datos de la cuenta con usuario y contraseña. La información almacenada cumple con los estándares de seguridad vigentes.

TYR dispone de un protocolo de seguridad establecido y que es seguido por todo el equipo de trabajo, para que al momento en que el usuario solicite sus datos le sean entregados o eliminados, según lo requiera.

Los datos que se recopilan dentro de TYR son todos los referentes al negocio. Esta información es recopilada para hacer uso de la plataforma, brindar soporte y efectuar el cobro cuando se toma un plan pago.

TYR utiliza herramientas e integraciones con terceros para el manejo de información que cumplen con el reglamento RGPD/GDPR con fines específicos sin derecho a ser compartidos o usados con un propósito distinto al que autorizó inicialmente el usuario. Ver más.

Los nuevos miembros del equipo de trabajo de TYR son capacitados al momento de su ingreso con todas las normas de seguridad sobre los datos que se almacenan en TYR, para un uso apropiado de esta información.

A la entrada en vigencia de esta norma fue nombrado en TYR un DPO (oficial de protección de datos) para garantizar que se cumpla con el reglamento, mantener un registro de entrenamiento al equipo de trabajo y a los procedimientos de seguridad establecidos. El DPO puede ser contactado en el correo electrónico privacidad@TYR.com.

VIII. Autorización del Titular para el Tratamiento de Datos Sensibles

En el Tratamiento de datos personales sensibles, LA EMPRESA cumplirá con las siguientes obligaciones:

Informar al titular que por tratarse de datos sensibles no está obligado a autorizar su tratamiento.

Informar al titular de forma explícita y previa, además de los requisitos generales de la autorización para la recolección de cualquier tipo de dato personal, cuáles de los datos que serán objeto de tratamiento son sensibles y la finalidad de su tratamiento y además obtener su consentimiento expreso.

Ninguna de las actividades que realiza LA EMPRESA está ni estará condicionada a que el titular suministre sus datos personales sensibles.

IX. Uso y Finalidad del Tratamiento de Datos Personales

LA EMPRESA reconoce que el Titular de los datos personales tiene derecho a contar con elementos adecuados que garanticen la misma, teniendo en todo caso para ello en cuenta sus responsabilidades, derechos y obligaciones.

LA EMPRESA recolecta, registra, almacena, usa los  datos personales de los Titulares, para su propio uso con los propósitos que fueron solicitados o por requerimientos de las entidades públicas.

Los Datos Personales de los Titulares son utilizados por LA EMPRESA para: • Ejecutar las actividades propias de LA EMPRESA para cumplir su objeto social, todo lo cual se hará con base en la finalidad de la Base de Datos en la que reposan los Datos Personales de los Titulares.  • Ofrecerle los productos, servicios y/o beneficios que buscan satisfacer las necesidades de los Titulares, o los productos y servicios de LA EMPRESA, lo cual puede hacerse por medios físicos o a través de correos electrónicos y/o terminales móviles.   • Enviar la información a entidades gubernamentales por exigencia legal. • Consultar información en las listas de control (Listas Nacionales e Internacionales), consultar a la CIFIN, a las centrales de información, Lista Clinton, Procuraduría, Contraloría,  Policía Nacional, DIJIN con el fin de preservar la confianza y transparencia entre el Titular de los Datos y LA EMPRESA • Soportar procesos de auditoría externa e interna. • Para la ejecución de procesos de índole judicial  y extrajudicial en los casos permitidos por los Estatutos y Reglamentos de LA EMPRESA. • Registrar la información de Empleados, ex empleados,  proveedores, clientes (activos e inactivos) en las bases de datos de LA EMPRESA, para el envío de información contractual, comercial y obligacional a que hubiere lugar. • Para verificación de referencias de empleados, ex empleados,  proveedores, clientes (activos e inactivos) en las bases de datos.  • Respecto de la recolección y tratamiento de datos realizados mediante mecanismos automatizados con el objeto de generar registros de actividad de los visitantes y registros de audiencia LA EMPRESA, sólo podrá utilizar dicha información para la elaboración de informes que cumplan con los objetivos señalados. En ningún caso podrá realizar operaciones que impliquen asociar dicha información a algún usuario identificado o identificable.

Los Datos Personales serán utilizados por LA EMPRESA sólo para los propósitos aquí señalados, por lo tanto, LA EMPRESA no venderá, licenciará, transmitirá o divulgar los Datos Personales, salvo que: • El Titular autorice expresamente a hacerlo  • La información del Titular  tenga relación con una fusión, consolidación, adquisición, desinversión u otro proceso de restructuración de LA EMPRESA • Sea permitido por la ley.

Para el manejo interno de los Datos, éstos podrán ser conocidos por el personal autorizado de LA EMPRESA, lo cual incluye la Asamblea General de Accionistas, la Revisoría Fiscal, las Gerencias.

LA EMPRESA podrá subcontratar a terceros para el procesamiento de determinadas funciones o información. Cuando ello ocurra, dichos terceros estarán obligados a proteger los Datos Personales en los términos exigidos por la ley y en su condición de Encargados del manejo de las Bases de Datos de LA EMPRESA.

En el caso de transmisión de datos personales, LA EMPRESA suscribió el contrato de transmisión a que haya lugar en los términos del Decreto 1074 de 2015.

Igualmente, LA EMPRESA podrá transferir o transmitir (según corresponda), guardando las debidas medidas de seguridad, los  datos personales a otras entidades en Colombia o en el extranjero para la prestación de un mejor servicio, de conformidad con las autorizaciones que hayan sido otorgadas por los Titulares de los datos personales.

Una vez cese la necesidad de Tratamiento de los Datos Personales, los mismos serán eliminados de las bases de datos de LA EMPRESA en términos seguros.

X. Aviso de Privacidad

Esta  leyenda se encuentra en todos los formularios o documentos por medio de los cuales se recolecta información de los proveedores, trabajadores, clientes y demás titulares de los datos personales que maneja LA EMPRESA. Cuando se recolectan de manera verbal, esta leyenda es comunicada al Titular de igual forma, y de la autorización se deja constancia a través de medios técnicos dispuestos para el efecto.

Aviso de Privacidad

Autorización: Para la página web

Autorización:  Para empleados

XI. Revocatoria de la Autorización y/o Suspención del Dato

LA EMPRESA ha dispuesto un mecanismo gratuito y ágil a través del cual el Titular puede en todo momento, y siempre que no medie un deber legal o contractual que así lo impida, solicitar a LA EMPRESA la supresión de los datos personales y/o revocar la autorización que  ha otorgado para el Tratamiento de los mismos, mediante la presentación de una solicitud.

Si vencido el término legal respectivo, LA EMPRESA no elimina de las bases de datos los  datos personales del Titular que lo solicitó, el Titular tendrá derecho a solicitar a la Superintendencia de Industria y Comercio que ordene la revocatoria de la autorización y/o la supresión de los datos personales.

XII. Derechos del Titular de la Información

El titular de los datos personales tendrá los siguientes derechos:

a) Conocer, actualizar y rectificar sus datos personales frente a TYR en su condición de responsable del tratamiento. Este derecho se podrá ejercer, entre otros, frente a datos parciales, inexactos, incompletos, fraccionados, que induzcan a error, o aquellos cuyo tratamiento esté expresamente prohibido o no haya sido autorizado. 

b) Solicitar prueba de la autorización otorgada a TYR salvo cuando expresamente se exceptúa como requisito para el tratamiento (casos en los cuales no es necesaria la autorización). 

c) Ser informado por TYR, previa solicitud, respecto del uso que le ha dado a sus datos personales. 

d) Presentar ante la Superintendencia de Industria y Comercio quejas por infracciones a lo dispuesto en la ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen. 

e) Revocar la autorización y/o solicitar la supresión del dato cuando en el Tratamiento no se respeten los principios, derechos y garantías constitucionales y legales. 

f) Acceder en forma gratuita a sus datos personales que hayan sido objeto de tratamiento.

XIII. Procedimiento para el Ejercicio de los Derechos del Titular de Datos

Los derechos de los Titulares, podrán ser ejercidos ante LA EMPRESA por las siguientes personas: a. Por el Titular de los datos, quien deberá acreditar ante LA EMPRESA su identidad en forma suficiente por los distintos medios o mecanismos que tenemos a su disposición.  b. Por los causahabientes del Titular de los datos, quienes deberán acreditar tal calidad ante LA EMPRESA.  c. Por el representante y/o apoderado del Titular de los datos, previa acreditación ante LA EMPRESA de la representación o apoderamiento. d. Por estipulación a favor de otro o para otro. De acuerdo con lo previsto en la ley cualquiera de los derechos que le asisten como Titular de los datos, usted podrá utilizar ante LA EMPRESA cualquiera de los mecanismos que se establecen a continuación:

1. Procedimiento para Consultas: - Los Titulares, sus causahabientes, sus representantes o apoderados, podrán consultar la información personal del Titular que repose en las base de datos de LA EMPRESA.  - LA EMPRESA como Responsable y/o Encargada del Tratamiento suministrará la información solicitada que se encuentre contenida en la base de datos o la que esté vinculada con la identificación del Titular. - El titular acreditará su condición mediante copia del documento pertinente y de su documento de identidad que podrá suministrar en medio físico o digital, en caso de que el titular esté representado por un tercero, deberá allegarse el respectivo poder, que deberá contener el respectivo contenido ante notario, el apoderado deberá igualmente acreditar su identidad en los términos indicados.. - La consulta se formulará a través de los canales que para dicho efecto han sido habilitados por LA EMPRESA y en especial a través de comunicación escrita o electrónica, dirigida a la dependencia y persona indicada en el presente Manual. - La consulta será atendida por LA EMPRESA en un término máximo de diez (10) días hábiles contados a partir de la fecha de recibo de la misma.  - Cuando no fuere posible para LA EMPRESA atender la consulta dentro de dicho término, lo informará al interesado, expresando los motivos de la demora y señalando la fecha en que atenderá su consulta, la cual en ningún caso superará los cinco (5) días hábiles siguientes al vencimiento del primer término.

Se podrán consultar de forma gratuita los datos personales al menos una vez cada mes calendario, y cada vez que existan modificaciones sustanciales de las Políticas establecidas en este Manual que motiven nuevas consultas.

Para consultas cuya periodicidad sea mayor a una por cada mes calendario, LA EMPRESA podrá cobrar al Titular los gastos de envío, reproducción y, en su caso, certificación de documentos.

2. Procedimiento para Reclamos: Los Titulares, sus causahabientes, sus representantes o apoderados, que consideren que la información que se encuentra contenida en las bases de datos de LA EMPRESA debe ser objeto de corrección, actualización o supresión, o cuando adviertan el presunto incumplimiento de cualquiera de los deberes contenidos en la ley, podrán presentar un reclamo ante LA EMPRESA como Responsable y/o Encargada del Tratamiento, el cual será tramitado bajo las siguientes reglas:  - El reclamo se formulará mediante solicitud escrita dirigida a LA EMPRESA, con la identificación del Titular, la descripción de los hechos que dan lugar al reclamo, la dirección, y acompañando los documentos que se quiera hacer valer. - Al reclamo deberá adjuntarse fotocopia del documento de identificación del Titular de los datos. - El reclamo se formulará a través de los canales que para dicho efecto han sido habilitados por LA EMPRESA, y se dirigirá a la dependencia y a la persona indicada en el presente Manual.
- Si el reclamo resulta incompleto, LA EMPRESA requerirá al interesado dentro de los cinco (5) días hábiles siguientes a la recepción del reclamo para que subsane las fallas.  - Transcurridos dos (2) meses desde la fecha del requerimiento realizado por LA EMPRESA, sin que el solicitante presente la información requerida, LA EMPRESA entenderá que se ha desistido del reclamo. - En caso que quien reciba el reclamo no sea competente para resolverlo, dará traslado a quien corresponda en un término máximo de dos (2) días hábiles e informará de la situación al interesado. - Una vez LA EMPRESA reciba el reclamo completo, incluirá en la base de datos una leyenda que indique: "reclamo en trámite" y el motivo del mismo, en un término no mayor a dos (2) días hábiles. Dicha leyenda deberá mantenerse hasta que el reclamo sea decidido. - El término máximo para atender el reclamo por parte de LA EMPRESA será de quince (15) días hábiles contados a partir del día siguiente a la fecha de su recibo.  - Cuando no fuere posible para LA EMPRESA atender el reclamo dentro de dicho término, se informará al interesado los motivos de la demora y la fecha en que se atenderá su reclamo, la cual en ningún caso podrá superar los ocho (8) días hábiles siguientes al vencimiento del primer término.
3. Canales Habilitados: Los derechos de los titulares podrán ser ejercidos por las personas antes señaladas a través de los canales que han sido habilitados por LA EMPRESA  para dicho efecto, los cuales se encuentran a su disposición de forma gratuita, así: - A través de la dirección de correo electrónico: soporte@TYR.com

XIV. Deberes de TYR como Responsable y Encargado del Tratamiento de los Datos Personales

Son deberes de TYR los siguientes, sin perjuicio de las disposiciones previstas en la ley:

a) Garantizar al titular, en todo tiempo, el pleno y efectivo ejercicio del derecho de hábeas data. 

b) Solicitar y conservar, copia de la respectiva autorización otorgada por el titular.

c) Informar debidamente al titular sobre la finalidad de la recolección y los derechos que le asisten en virtud de la autorización otorgada. 

d) Conservar la información bajo las condiciones de seguridad necesarias para impedir su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento. 

e) Garantizar que la información sea veraz, completa, exacta, actualizada, comprobable y comprensible. 

f) Actualizar la información, atendiendo de esta forma todas las novedades respecto de los datos del titular. 

g) Rectificar la información cuando sea incorrecta y comunicar lo pertinente. 

h) Respetar las condiciones de seguridad y privacidad de la información del titular. 

i) Tramitar las consultas y reclamos formulados en los términos señalados por la ley. 

j) Identificar cuando determinada información se encuentra en discusión por parte del titular. 

k) Informar a solicitud del titular sobre el uso dado a sus datos. 

l) Informar a la autoridad de protección de datos cuando se presenten violaciones a los códigos de seguridad y existan riesgos en la administración de la información de los titulares. 

m) Cumplir los requerimientos e instrucciones que imparta la Superintendencia de Industria y Comercio sobre el tema en particular. 

n) Usar únicamente datos cuyo tratamiento esté previamente autorizado de conformidad con lo previsto en la ley 1581 de 2012. 

o) Hacer uso de los datos personales del titular solo para aquellas finalidades para las que se encuentre facultada debidamente y respetando en todo caso la normatividad vigente sobre protección de datos personales.

XV. Medidas de Seguridad de la Información Aplicadas al Tratamiento de Base de Datos

TYR adoptará las medidas técnicas, humanas y administrativas que sean necesarias para otorgar seguridad a los registros evitando su adulteración, pérdida, consulta, uso o acceso no autorizado o fraudulento. El personal que realice el tratamiento de los datos personales ejecutará los protocolos establecidos con el fin de garantizar la seguridad de la información.

Para este fin, cuenta con protocolos de seguridad y acceso a los sistemas de información, almacenamiento y procesamiento incluidas medidas físicas de control de riesgos de seguridad. Permanentemente se realiza monitoreo al sistema a través de análisis de vulnerabilidades.

El acceso a las diferentes bases de datos se encuentra restringido incluso para los empleados y colaboradores.

Todos los funcionarios han suscrito cláusulas de confidencialidad en sus contratos y están comprometidos con la manipulación adecuada de las bases de datos atendiendo a los lineamientos sobre tratamiento de la información establecida en la Ley.

La información personal suministrada por los usuarios de TYR, está asegurada por una clave de acceso a la cual sólo el Usuario puede acceder y que sólo él conoce; el Usuario es el único responsable del manejo de dicha clave. TYR no tiene acceso ni conoce la referida clave, todas las claves de usuarios se encuentran encriptadas. Para mayor seguridad, TYR recomienda a los usuarios de nuestros portales el cambio periódico de su contraseña de ingreso.

Es responsabilidad del usuario tener todos los controles de seguridad en sus equipos o redes privadas para su navegación hacia nuestros portales.

TYR ha implementado todos los mecanismos de seguridad vigentes en el mercado acordes con sus productos. Además, ha desplegado una serie de documentos y actividades a nivel interno para garantizar el correcto funcionamiento de los esquemas de seguridad técnica; no obstante, a pesar de la debida diligencia adoptada, el TYR no se responsabiliza por cualquier consecuencia derivada del ingreso indebido o fraudulento por parte de terceros a la base de datos y/o por alguna falla técnica en el funcionamiento. Los presentes lineamientos son aplicables de cumplimento obligatorio para los portales web propiedad de TYR, y de aquellos que a futuro se adquieran o se desarrollen.

XVI. Prohibiciones

Se establecen las siguientes prohibiciones y sanciones como consecuencia de su incumplimiento a la normatividad: Prohibiciones

XVII. Responsable de la Atención de Peticiones, Consultas y Reclamos

La responsabilidad en el  tratamiento de los datos personales en TYR  está en cabeza de todos los empleados, al interior de cada área deberán adoptar las reglas y procedimientos para la aplicación y cumplimiento de la norma, dada su condición de custodios de la información personal que está contenida en los sistemas de información de LA EMPRESA.

TYR ha designado  el área de Administrativa como responsable del cumplimiento de esta política y estará atenta para resolver peticiones, consultas, y reclamos  y para realizar cualquier actualización, rectificación, supresión  y revocatoria de autorización de manejo de datos personales, a través de los canales dispuestos para tal efecto.

XVIII. Transferencia y Transmisión Internacional de Datos Personales

TYR Tecnología y resultados .podrá efectuar transferencia y transmisión de datos personales de los titulares. Para la transferencia internacionales de datos personales de los titulares TYR tomará las medidas necesarias para que los terceros conozcan y se comprometan a observar esta Política, bajo el entendido que la información personal que reciban, únicamente podrá ser utilizada para asuntos directamente relacionados con LA EMPRESA y solamente mientras ésta dure y no podrá ser usada o destinada para propósito o fin diferente. Para la transferencia internacional de datos personales se observará lo previsto en el artículo 26 de la Ley 1581 de 2012.

Con la aceptación de la presente política, el Titular autoriza expresamente para transferir y transmitir Información Personal. La información será transferida y transmitida, para todas las relaciones que puedan establecerse con TYR. En conformidad con la Ley 1581 de 2012, informamos que los datos suministrados por los usuarios serán registrados en nuestros servidores que se encuentran alojados por fuera del territorio colombiano. Al aceptar los términos y condiciones de uso de la plataforma el usuario autoriza a Seguridad en Línea al tratamiento de sus datos para efectos nombrados en la presente política.

XIX. Fecha de Entrada en Vigencia

La presente Política de Datos Personales fue creada el día 11 de octubre de 2016 y entra en vigencia a partir del día 15 de mayo de 2017. Cualquier cambio que se presente respecto de la presente política, se informará a través de la dirección electrónica: www.tyrlabx.com

SEGUNDA PARTE

POR FAVOR LEA ATENTAMENTE LA SIGUIENTE LICENCIA DE USO Y EVALUACIÓN PERSONAL ANTES DE DESCARGAR O USAR EL SOFTWARE TYR - Tecnología y Resultados. ESTOS TÉRMINOS Y CONDICIONES CONSTITUYEN UN ACUERDO LEGAL ENTRE USTED Y TYR - Tecnología y Resultados.

TYR - Tecnología y Resultados Colombia,. (“TYR - Tecnología y Resultados”) ESTÁ DISPUESTO A OTORGAR LA LICENCIA DEL PRODUCTO DEFINIDO EN LA SECCIÓN 1 A CONTINUACIÓN SOLAMENTE CON LA CONDICIÓN DE QUE ACEPTE TODOS LOS TÉRMINOS CONTENIDOS EN ESTE ACUERDO DE LICENCIA DE USO PERSONAL Y EVALUACIÓN DEL SOFTWARE (“ACUERDO“).

SI ACEPTA ESTA LICENCIA EN NOMBRE DE UNA ENTIDAD (EN LUGAR DE COMO SER HUMANO INDIVIDUAL), DECLARA QUE TIENE LA AUTORIDAD APROPIADA PARA ACEPTAR ESTOS TÉRMINOS Y CONDICIONES EN NOMBRE DE DICHA ENTIDAD.

1. Sujeto de acuerdo. Este Acuerdo rige el uso del paquete de software denominado “TYR - Tecnología y Resultados solution“ (el “Producto“), que contiene un conjunto de funciones adicionales para “TYR - Tecnología y resultados“ que mejoran la operación de servicios como historia clínica electrónica y/o facturación electrónica. El Producto consta de archivos ejecutables en código de máquina, archivos de script, archivos de datos y toda la documentación y actualizaciones que TYR - Tecnología y Resultados le proporcione.

2. Concesión de la licencia. TYR - Tecnología y Resultados le otorga una licencia personal, no exclusiva, intransferible y limitada sin cargos para reproducir, instalar, ejecutar y usar internamente el Producto en computadoras anfitrionas para su uso personal, uso educativo o evaluación. “Uso personal“ es un uso no comercial únicamente por la persona que descarga el Producto de TYR - Tecnología y Resultados en un único equipo host, siempre que no se conecte más de un cliente o equipo remoto a ese equipo host y que se use un equipo cliente o remoto únicamente para ver de forma remota las Computadoras invitadas. El “Uso educativo“ sirve para los profesores o estudiantes en una institución académica (escuelas, institutos y universidades) como parte del plan de estudios de la institución. “Evaluación“ significa probar el Producto por hasta treinta (30) días; una vez transcurrido ese plazo, ya no podrá utilizar el Producto. El uso personal y / o educativo excluye expresamente cualquier uso del producto con fines comerciales o para operar, ejecutar o actuar en nombre o en beneficio de una empresa, organización, organización gubernamental o institución educativa. TYR - Tecnología y Resultados se reserva todos los derechos no expresamente otorgados en esta licencia.

3. Restricciones y Reserva de Derechos. 

1. El Producto y las copias del mismo que se le proporcionen en virtud de este Acuerdo están sujetos a derechos de autor y se les concede una licencia, no se los vende, por TYR - Tecnología y Resultados.

2. No puede hacer nada de lo siguiente: (a) modificar cualquier parte del Producto, excepto en la medida permitida en la documentación que acompaña al Producto; (b) alquilar, arrendar, prestar, re-distribuir o gravar el Producto; (c) eliminar o alterar leyendas o avisos de propiedad contenidos en el Producto; o (d) descompilar, o aplicar ingeniería inversa al Producto (excepto en la medida permitida por la ley aplicable).

3. El Producto no está diseñado, licenciado o destinado para su uso en el diseño, construcción, operación o mantenimiento de ninguna instalación nuclear y TYR - Tecnología y Resultados y sus licenciantes niegan cualquier garantía expresa o implícita de idoneidad para tales usos.

4. No se otorga ningún derecho, título o interés en ninguna marca comercial, marca de servicio, logotipo o nombre comercial de TYR - Tecnología y Resultados o sus licenciantes bajo este Acuerdo.

5. Terminación. El Acuerdo entra en vigencia en la fecha en que recibe el Producto y permanece vigente hasta que sea terminado. Sus derechos bajo este Acuerdo terminará inmediatamente sin previo aviso por parte de TYR - Tecnología y Resultados si usted incumple materialmente o toma alguna medida en derogación de TYR - Tecnología y Resultados y / o los derechos de sus licenciantes sobre el Producto. TYR - Tecnología y Resultados puede rescindir el presente Contrato en caso de que alguna parte del Producto se convierta en una opinión razonable de TYR - Tecnología y Resultados susceptible de ser objeto de una reclamación por infracción de la propiedad intelectual o apropiación indebida de secretos comerciales. Tras la terminación, dejará de usar y destruirá todas las copias del Producto bajo su control y confirmará el cumplimiento por escrito a TYR - Tecnología y Resultados. Las Secciones 3-9, inclusive, sobrevivirán a la terminación del Acuerdo.

6. Renuncia de garantía. EN LA MEDIDA EN QUE NO ESTÉ PROHIBIDO POR LA LEY APLICABLE, TYR - Tecnología y Resultados PROPORCIONA EL PRODUCTO “TAL CUAL“ SIN GARANTÍA DE NINGUNA CLASE, YA SEA EXPRESA O IMPLÍCITA. SIN LIMITAR LO ANTERIOR, TYR - Tecnología y resultados NIEGA ESPECÍFICAMENTE CUALQUIER GARANTÍA IMPLÍCITA DE COMERCIABILIDAD, IDONEIDAD PARA UN PROPÓSITO PARTICULAR, TÍTULO Y NO INFRACCIÓN. El riesgo total en cuanto a la calidad y el rendimiento del Producto está con usted. Si se comprueba que es defectuoso, usted asume el costo de todos los servicios, reparaciones o correcciones necesarias.

7. Limitación de responsabilidad. EN LA MEDIDA QUE NO ESTÉ PROHIBIDO POR LA LEY APLICABLE, EN NINGÚN CASO TYR - Tecnología y Resultados O SUS OTORGANTES DE LICENCIAS SERÁN RESPONSABLES DE CUALQUIER INGRESO, GANANCIA, DATOS O USO DE DATOS PERDIDOS O DE DAÑOS ESPECIALES, INDIRECTOS, CONSECUENTES, INCIDENTALES O PUNITIVOS, SIN EMBARGO CAUSADO INDEPENDIENTEMENTE DE LA TEORÍA DE RESPONSABILIDAD, DERIVADA DEL USO O INCAPACIDAD DE USO DEL PRODUCTO, AUN CUANDO TYR - Tecnología y Resultados HAYA SIDO ADVERTIDA DE LA POSIBILIDAD DE DICHOS DAÑOS. En ningún caso la responsabilidad de TYR - Tecnología y Resultados hacia usted, ya sea por contrato, responsabilidad extracontractual (incluida la negligencia), o de otro modo, excederá la cantidad pagada por usted por el Producto bajo este Acuerdo.

8. Tecnología de terceros con licencia separada. El Producto puede contener o requerir el uso de Tecnología de terceros que se proporciona con el Producto. TYR - Tecnología y Resultados puede proporcionarle ciertos avisos en la documentación del Producto, archivos de registro o archivos de aviso en relación con dicha Tecnología de terceros. Se le otorgara una licencia de Tecnología de terceros, ya sea bajo los términos de este Acuerdo o, si se especifica en la documentación, archivos readme o avisos, en Términos separados. Sus derechos para usar Tecnología de terceros con licencia separada bajo Términos separados no están restringidos de ninguna manera por este Acuerdo. Sin embargo, para mayor claridad, a pesar de la existencia de un aviso, la Tecnología de terceros que no sea una Tecnología de terceros con Licencia Separada se considerará parte del Producto y se le concede la licencia según los términos de este Acuerdo. “Términos separados“ hace referencia a los términos de licencia separados que se especifican en la documentación del Producto, los archivos readme o de aviso y que se aplican a la Tecnología de terceros con licencia separada. “Tecnología de terceros con licencia separada“ se refiere a la Tecnología de terceros que está autorizada bajo Términos separados y no bajo los términos de este Acuerdo.

9. Exportar. Las leyes y regulaciones de exportación de los Estados Unidos y cualquier otra ley y reglamentación local relevante de exportación se aplican al Producto. Usted acepta que dichas leyes de exportación rigen el uso del Producto (incluidos los datos técnicos) proporcionados en virtud de este Acuerdo, y usted acepta cumplir con todas las leyes y regulaciones de exportación (incluidas las reglamentaciones de “exportación estimada“ y “re-exportación estimada“). Usted acepta que ningún dato, información y / o producto (o producto directo del mismo) será exportado, directa o indirectamente, en violación de estas leyes, o será utilizado para cualquier propósito prohibido por estas leyes, incluyendo, sin limitación, nuclear, proliferación de armas químicas o biológicas, o desarrollo de Tecnología de misiles.

10. Los usuarios finales. Los programas de TYR - Tecnología y Resultados, incluido el Producto, cualquier sistema operativo, software integrado, cualquier programa instalado en hardware y / o documentación entregado a los usuarios finales del Gobierno. Son “software informático comercial“ de conformidad con el Reglamento y la Administración de Adquisiciones regulaciones suplementarias específicas. Como tal, el uso, duplicación, divulgación, modificación y adaptación de los programas, incluyendo cualquier sistema operativo, software integrado, cualquier programa instalado en el hardware y / o documentación, estarán sujetos a términos de licencia y restricciones de licencia aplicables a los programas. No se otorgan otros derechos al gobierno de Colombia.

11. Incremento. El valor del incremento pactado a las tarifas base de cada uno de los servicios será considerado anualmente como referencia al IPC + 2 %. Dicho esto la parte de TYR - Tecnología de servicio se reserva el derecho de aumento a este incremento en caso de que as lo considere necesario para la continua satisfacción de su operación. Al igual que el incumplimiento de cualquiera de los enunciados en este documento por parte del cliente dará causalidad de terminación inmediata del servicio si as es considerado por la parte TYR - Tecnología y resultados.

12. Diverso. Este acuerdo es el acuerdo completo entre usted y TYR - Tecnología y Resultados en relación con su contenido. Reemplaza todas las comunicaciones, propuestas, declaraciones y garantías previas o simultáneas orales y escritas y prevalece sobre cualquier término conflictivo o adicional de cualquier cotización, orden, reconocimiento u otra comunicación entre las partes relacionada con su tema durante el término de este Acuerdo. Ninguna modificación de este Acuerdo será vinculante, a menos que sea por escrito y esté firmada por un representante autorizado de cada parte. Si se considera que ninguna disposición de este Acuerdo es inaplicable, este Acuerdo seguirá vigente con la disposición emitida, a menos que la omisión frustrara la intención de las partes, en cuyo caso este Acuerdo terminará inmediatamente. 

Fecha última modificación: 22 Enero de 2025
Atentamente,

TYR Tecnología y resultados## my friend
