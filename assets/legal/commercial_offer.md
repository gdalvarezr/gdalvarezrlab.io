<img src="https:/tyrlabx.com/assets/logos/logo_300x150.png" title="" alt="logo_500x300.png" width="203">

Bogotá D.C, 2025.

Señores (as)

Respetado(s) Señor(es)

Reciban un cordial saludo TYR, agradecemos la oportunidad que nos brinda para convertirnos en su proveedor de servicios de tecnología.

Presentamos a su consideración la propuesta comercial que se ajusta a sus necesidades. En caso de que requiera aclaración o ampliación de información respecto de los servicios ofrecidos por nuestra software, estamos atentos a suministrar.

Esperamos de esta manera convertirnos en su aliado y principal alternativa.

## ACUERDO DE CONFIDENCIALIDAD

La presente propuesta ha sido preparada para el uso exclusivo de el interesado en atención a la solicitud que formuló a TYR, con base en lo anterior y al carácter confidencial al que refiere la Ley 23 de 1982 y según lo dispuesto en el Artículo 260 de la Decisión 486 de 2000 de la Comisión del Acuerdo de Cartagena sobre secreto empresarial. El contenido de este documento es CONFIDENCIAL y de uso exclusivo para el interesado, no debe ser revelado fuera de la organización y no debe ser utilizado para fines diferentes al establecido, su divulgación pública a partes no interesadas o a un competidor, resultará en perjuicio para TYR, si es que previamente no ha mediado autorización
escrita de TYR.

En caso de que se tenga prueba de que se ha divulgado esta información que es CONFIDENCIAL, la parte que realizó dicha divulgación indemnizará a la otra por los perjuicios que con tal hecho ocasione. Esta propuesta constituye la única oferta formal de TYR y prevalece sobre cualquier comunicación, opinión, entendimiento o acuerdo previo o posterior a esta. Cualquier modificación a esta propuesta, se hará mediante documento escrito.

## DESCRIPCIÓN DEL SERVICIO

Este aplicativo cuenta con los módulos que se requieren para el correcto funcionamiento de una Institución prestadora de servicios de salud (IPS) en sus modalidades, en sus ámbitos (ambulatorio) e independiente de la empresa administradora de planes de beneficios (EAPB). Dentro de sus múltiples módulos cabe destacar algunos como facturación (permite facturar servicios adicionales prestados a

cualquier empresa administradora de planes de beneficios (EAPB) y entes territoriales), caja, cartera, pagarés, RIPS, cuenta con el registro y control de sus procesos asistenciales y administrativos de forma eficiente y oportuna siguiendo la ruta de atención del paciente en los ámbitos que se requieran.

TYR es un software de historia clínica electrónica para hospitales, clínicas y consultorios que pone al alcance de su modelo operativo y de negocio, una solución tecnológica basada en la metodología de tres capas y estándares internacionales como CIE-10, que le permite la captura y tratamiento de la información clínica de sus pacientes para que, a través de herramientas de inteligencia de negocios, pueda realizar una correcta gestión del riesgo. También podrá parametrizar y administrar la herramienta de acuerdo a sus necesidades y especialidades médicas sin depender de personal técnico de soporte de nuestro software.

Además, TYR está integrado con Tryton ERP, una poderosa herramienta que facilita la facturación. Esto elimina integraciones con otros productos lo que reduce costos y da mayores ventajas al software de historia clínica.

A través de este software de historia clínica electrónica se facilita la operatividad de sus procesos asistenciales y administrativos, reduciendo significativamente los costos operacionales y garantizando la disponibilidad, acceso, archivo, seguridad y recuperación de su información.

Canales de atención:

    - Solicitud vía correo electrónico a contacto@tyrlabx.com
    - Chat en línea https://tyrlabx.com

Nota. El acompañamiento en la configuración de todos los servicios será a través de medios electrónicos y será atendido de forma remota.

El horario de atención del servicio de soporte es:

    - Lunes a viernes 9:00 a 16:00

## PROPUESTA ECONÓMICA

[Precios actualizados](https://tyrlabx.com/index.html#plans)

<iframe src="https://tyrlabx.com/index.html#plans" width="900" height="800" frameborder="0"></iframe>

Puedes validar los planes disponibles en nuestra página web y las características de cada uno , las cuales serán aplicables a este documento: Planes ofertados tyr



<iframe src="https://tyrlabx.com/index.html#plans" width="900" height="800" frameborder="0"></iframe>


| Item | Descripción  | Precio     |
| ---- | ------------ | ---------- |
| 1    | Capacitación | $150.000/h |
| 2    | Conferencia  | $150.000/h |
| 3    | Chat/Mail    | $0         |

## CONDICIONES COMERCIALES

Validez de la oferta: Esta oferta tendrá una validez de treinta (15) días calendario.

Para Pago electrónico

[Datáfono virtual]([Wompi](https://checkout.wompi.co/l/VPOS_PfRtyE))

Quedamos atentos y a su disposición para aclarar cualquier información adicional que se requiera y/o resolver las inquietudes frente a lo descrito en la presente propuesta comercial, recuerde que para nosotros siempre es un placer atenderle.

## CONSIDERACIONES FINALES

- Los términos y condiciones de uso del software no serán negociables y sí de aceptación obligatoria para el uso del software los cuales se adjuntan a esta oferta.

- Todo el proyecto se trabaja de manera virtual y no presencial, no incluye gastos de desplazamiento, hospedaje, alimentación y viáticos del equipo de trabajo si es

- Facturación 50% para iniciar y 50% a cierre o con hitos parciales definidos del proyecto.

- La modalidad de registro del software ante la DIAN es software propio.

- La publicación en WEB se hará con un subdominio de tyrlabx.com (Configurar con dominio propio del cliente es posible y podría tener costos adicionales como el costo del dominio o certificado).

- Esta propuesta se presenta teniendo en cuenta la información recibida, por parte del cliente si al momento de implementar nos encontramos con diferencias importantes que dificultan o requieren mayores tiempos para su implementación se presentaran y cobraran adicionalmente.

- El soporte estándar incluido es 5x8 la atención a sus consultas se hará principalmente vía chat y correo electrónico, en ningún caso incluye visitas de nuestro equipo a el cliente o del cliente a nuestro equipo.

- Se realizará acompañamiento hasta el proceso de la primera factura en ambiente de producción, procesos posteriores de ser necesario se aplicará el proceso de soporte del plan seleccionado.

- El costo de los servicios es mensual o anual si el cliente así lo desea para obtener mayores beneficios.

- La integridad, configuración de clientes, precios, o cualquier dato inherente a la empresa adquiriente del uso del software será responsabilidad única y exclusivamente de esta, es decir, del cliente.

- El costo del soporte es opcional, el cliente decide si adquirirlo o no fuera del contratado en el plan descrito.

- La integridad de información ingresada en el sistema es de total administración y responsabilidad del cliente, es decir, es cliente es el único responsable de los datos, montos, impuestos, accesos, custodias, configuraciones, verificación y generación de documentos electrónicos que se realicen en el sistema y sean enviados a la DIAN y bajo ninguna circunstancia el proveedor de la solución de facturación será responsable de información, uso u otro tipo de fin que se le dé al sistema. (ver términos y condiciones para ampliar esta cláusula)

- El buen uso del sistema y diligenciamiento de datos en el orden y procedimiento correcto es mandatorio, al no seguir estas reglas se podrá generar mal funcionamiento, errores y por tanto incongruencias con los datos que se desean validar por la DIAN. Es altamente sugerido revisar la información del sistema contra la que queda en firme en el portal de la DIAN en caso de algún problema o duda, se recuerda al cliente que la última palabra sobre los documentos electrónicos es aquella que se ve en la plataforma de la DIAN.

- La implementación del proyecto será máximo de 1-2 semanas contadas a partir de la recepción del certificado electrónico que el cliente debe adquirir con un proveedor acreditado por la ONAC:

Cordialmente,
TYR Tecnología y resultados contacto@tyrlabx.com

**IMPORTANTE**. Recuerde que en caso de tener peticiones, quejas, reclamos, sugerencias, apelaciones, felicitaciones o detectar casos de imparcialidad relacionados con la prestación del servicio recibido, escribanos al correo electrónico contacto@tyrlabx.com o diríjase al formulario dispuesto en la página web https://tyrlabx.com
